﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

	public int startGold = 0;
	public int startLive = 0;
	public int destroyTowers = 1;
	public bool gameOver = false;

	public Text goldLabel;
	public Text liveLabel;
	public Text waveLabel;
	public int TotalWave;

	public GameObject destroyedTower;

	public GameObject endGamePanel;

	private AudioSource audio;

	private void Start()
	{
		Gold = startGold;
		Live = startLive;
		Wave = 0;

		audio = gameObject.GetComponent<AudioSource>();
		Random.InitState(42);
	}

	private int gold;
	public int Gold
	{
		get
		{
			return gold;
		}
		set
		{
			gold = value;
			goldLabel.text = gold.ToString();
		}
	}

	private int live;
	public int Live
	{
		get
		{
			return live;
		}
		set
		{
			live = value;

			if(live <= 0)
			{
				endGamePanel.SetActive(true);
			}

			liveLabel.text = live.ToString();
		}
	}

	private int wave;
	public int Wave
	{
		get
		{
			return wave;
		}
		set
		{
			wave = value;

			//destroy the towers
			if(value != 0)
			{
				for (int i = 0; i < destroyTowers; i++)
				{

					var towers = GameObject.FindGameObjectsWithTag("AvailableTower");

					if (towers.Length > 0)
					{
						int towerIndex = Random.Range(0, towers.Length - 1);
						ReplaceTower(towers[towerIndex]);
					}
				}
			}


			waveLabel.text = (wave+1).ToString();
		}
	}

	public void WinGame()
	{
		var endText = GameObject.FindGameObjectWithTag("EndText");
		endText.GetComponent<Text>().text = "You win";
		endGamePanel.SetActive(true);
	}

	private void ReplaceTower(GameObject towerToDestroy)
	{
		//create new tower
		var tower = Instantiate(destroyedTower, towerToDestroy.transform.position, Quaternion.identity);
		tower.transform.SetParent(towerToDestroy.transform.parent);

		if (audio != null)
		{
			AudioSource.PlayClipAtPoint(audio.clip, tower.transform.position, 1f);
		}

		towerToDestroy.SetActive(false);
		Destroy(towerToDestroy, 2);
	}
}
