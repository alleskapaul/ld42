﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	 public Button retryButton, quitButton;

	void Start()
	{
		//Button btn1 = retryButton.GetComponent<Button>();
		//Button btn2 = quitButton.GetComponent<Button>();

		//Calls the TaskOnClick/TaskWithParameters method when you click the Button
		retryButton.onClick.AddListener(Retry);
		quitButton.onClick.AddListener(Quit);
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void Retry()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
