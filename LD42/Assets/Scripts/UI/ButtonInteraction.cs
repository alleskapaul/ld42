﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInteraction : MonoBehaviour {

	public ButtonAction action;
	public Text valueLabel;
	public SpriteRenderer iconRenderer;

	public GameObject parentTower;
	private GameManager gameManager;
	private AudioSource audio;

	private void Start()
	{
		valueLabel.text = action.value.ToString();
		iconRenderer.sprite = action.icon;
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		audio = gameObject.GetComponent<AudioSource>();
	}

	private void OnMouseUp()
	{
		if (gameManager.Gold < action.value) return;

		//create new tower
		var tower = Instantiate(action.TowerPrefab, parentTower.transform.position, Quaternion.identity);
		tower.transform.SetParent(parentTower.transform.parent);
		
		if (audio != null)
		{
			AudioSource.PlayClipAtPoint(audio.clip, tower.transform.position, 1);
		}

		gameManager.Gold -= action.value;
		parentTower.SetActive(false);
		Destroy(parentTower, 2);
	}
}
