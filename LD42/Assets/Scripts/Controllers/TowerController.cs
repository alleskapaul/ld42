﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour {

	public GameObject ContextMenu;

	public bool canBuild = true;
	public bool isAttackTower;
	public bool isContextActive = false;
	public bool rotateTower = true;

	public float range = 0;
	public int damage = 0;
	public float attackRate = 0;
	public GameObject Icon;

	public GameObject bullet;
	public GameObject RangeRenderer;

	private float lastShotTime = 0;
	private bool showRange = false;

	private void Start()
	{
		lastShotTime = Time.time;
		if(ContextMenu != null)
		{
			var buttons = ContextMenu.GetComponentsInChildren<ButtonInteraction>();
			foreach (var button in buttons)
			{
				button.parentTower = gameObject;
			}
		}
		
		if(RangeRenderer != null)
		{
			RangeRenderer.SetActive(showRange);
			RangeRenderer.transform.localScale = new Vector3(range * 8, range * 8);
		}
		
	}

	private void FixedUpdate()
	{
		if (!isAttackTower) return;

		var enemies = GameObject.FindGameObjectsWithTag("Enemy");

		//check if an enemy is within range
		foreach(var enemy in enemies)
		{
			if (Vector3.Distance(enemy.transform.position, transform.position) <= range)
			{
				////within range so look at the target
				if (rotateTower)
				{
					Vector3 direction = Icon.transform.position - enemy.transform.position;
					Icon.transform.rotation = Quaternion.AngleAxis(
						Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI,
						new Vector3(0, 0, 1));
				}

				Attack(enemy.transform);

				return;
			}
		}
		
	}

	private void OnMouseUp()
	{
		if (!canBuild) return;

		isContextActive = !isContextActive;
		ContextMenu.SetActive(isContextActive);

		if(isAttackTower)
		{
			TogleRange();
		}
		
	}

	void Attack(Transform target)
	{
		if (Time.time - lastShotTime < attackRate) return;

		lastShotTime = Time.time;

		Vector3 startPosition = transform.position;
		Vector3 targetPosition = target.position;
		startPosition.z = bullet.transform.position.z;
		targetPosition.z = bullet.transform.position.z;

		GameObject newBullet = Instantiate(bullet);
		newBullet.transform.position = startPosition;
		IAttackController attackController = newBullet.GetComponent<IAttackController>();
		attackController.Target = target.gameObject;
		attackController.StartPosition = startPosition;
		attackController.TargetPosition = targetPosition;
		attackController.Damage = damage;
		attackController.Range = range;
	}

	void TogleRange()
	{
		showRange = !showRange;
		RangeRenderer.SetActive(showRange);
	}
}
