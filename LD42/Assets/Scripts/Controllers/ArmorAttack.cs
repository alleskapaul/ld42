﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorAttack : MonoBehaviour, IAttackController
{
	public GameObject Target { get; set; }
	public Vector3 StartPosition { get; set; }
	public Vector3 TargetPosition { get; set; }
	public int Damage { get; set; }
	public float Range { get; set; }

	private float startTime;
	private GameManager gameManager;
	private Animator animator;

	// Use this for initialization
	void Start()
	{
		startTime = Time.time;
		GameObject gm = GameObject.Find("GameManager");
		gameManager = gm.GetComponent<GameManager>();
		animator = GetComponent<Animator>();

		//rotate the sword to the enemy
		Vector3 direction = StartPosition - TargetPosition;
		transform.rotation = Quaternion.AngleAxis(
			Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI,
			new Vector3(0, 0, 1));

		transform.localScale = new Vector3(Range * 8, Range * 8);
	}

	// Update is called once per frame
	void FixedUpdate()
	{

		//get all targets within range
		var enemies = GameObject.FindGameObjectsWithTag("Enemy");

		//check if an enemy is within range
		foreach (var enemy in enemies)
		{
			if (Vector3.Distance(enemy.transform.position, StartPosition) <= Range)
			{
				if (enemy != null)
				{
					var enemyController = enemy.GetComponent<EnemyController>();
					enemyController.Health -= Damage;
					if (enemyController.Health <= 0 && enemyController.isActiveAndEnabled)
					{

						AudioSource audioSource = Target.GetComponent<AudioSource>();
						AudioSource.PlayClipAtPoint(audioSource.clip, transform.position);

						Target.SetActive(false);
						Destroy(Target, 2);

						gameManager.Gold += enemyController.value;
					}
				}
			}
		}

		//if (animator.GetCurrentAnimatorStateInfo(0).IsName("SlashAttack"))
		//{
		Destroy(gameObject, 1);
		//}
	}
}
