﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashAttackController : MonoBehaviour, IAttackController
{
	public GameObject Target { get; set; }
	public Vector3 StartPosition { get; set; }
	public Vector3 TargetPosition { get; set; }
	public int Damage { get; set; }
	public float Range { get; set; }

	private float startTime;
	private GameManager gameManager;
	private Animator animator;

	// Use this for initialization
	void Start()
	{
		startTime = Time.time;
		GameObject gm = GameObject.Find("GameManager");
		gameManager = gm.GetComponent<GameManager>();
		animator = GetComponent<Animator>();

		//rotate the sword to the enemy
		Vector3 direction = StartPosition - TargetPosition;
		transform.rotation = Quaternion.AngleAxis(
			Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI,
			new Vector3(0, 0, 1));

		transform.position = TargetPosition;
	}

	// Update is called once per frame
	void FixedUpdate () {

		if (Target != null)
		{
			var enemy = Target.GetComponent<EnemyController>();
			enemy.Health -= Damage;
			if (enemy.Health <= 0 && enemy.isActiveAndEnabled)
			{

				AudioSource audioSource = Target.GetComponent<AudioSource>();
				AudioSource.PlayClipAtPoint(audioSource.clip, transform.position);

				Target.SetActive(false);
				Destroy(Target, 2);

				gameManager.Gold += enemy.value;
			}
		}

		//if (animator.GetCurrentAnimatorStateInfo(0).IsName("SlashAttack"))
		//{
		Destroy(gameObject, 1);
		//}
	}
}
