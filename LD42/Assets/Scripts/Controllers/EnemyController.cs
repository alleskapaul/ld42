﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour {

	[HideInInspector]
	public Transform[] waypoints;
	private int currentWaypoint = 0;
	private float lastWaypointSwitchTime;
	public float speed = 1.0f;
	public float health = 0f;

	public float Health
	{
		get
		{
			return health;
		}
		set
		{
			health = value;
			healthBar.currentHealth = health;
		}
	}

	public float maxHealth = 0f;
	public int value = 50;
	public HealthBar healthBar;

	//private NavMeshAgent agent;

	private GameManager gameManager;

	private void Start()
	{
		//agent = GetComponent<NavMeshAgent>();
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		healthBar = GetComponentInChildren<HealthBar>();
		healthBar.maxHealth = maxHealth;
		healthBar.currentHealth = health;
		gameObject.transform.position = waypoints[0].position;
	}

	//void GotoNextPoint()
	//{
	//	// Returns if no points have been set up
	//	if (waypoints.Length == 0)
	//		return;

	//	// Set the agent to go to the currently selected destination.
	//	agent.destination = waypoints[currentWaypoint].position;

	//	// Choose the next point in the array as the destination,
	//	// cycling to the start if necessary.
	//	currentWaypoint = (currentWaypoint + 1) % waypoints.Length;

	//}


	//void Update()
	//{
	//	// Choose the next destination point when the agent gets
	//	// close to the current one.
	//	if (!agent.pathPending && agent.remainingDistance < 0.5f)
	//	{
	//		if (currentWaypoint < waypoints.Length - 2)
	//		{
	//			GotoNextPoint();
	//		}
	//		else
	//		{
	//			Destroy(gameObject);

	//			AudioSource audioSource = gameObject.GetComponent<AudioSource>();
	//			AudioSource.PlayClipAtPoint(audioSource.clip, transform.position);

	//			gameManager.Live--;

	//		}
	//	}

	//}

	void FixedUpdate()
	{
		Vector3 startPosition = waypoints[currentWaypoint].position;
		Vector3 endPosition = waypoints[currentWaypoint + 1].position;

		float pathLength = Vector3.Distance(startPosition, endPosition);
		float totalTimeForPath = pathLength / speed;
		float currentTimeOnPath = Time.time - lastWaypointSwitchTime;
		gameObject.transform.position = Vector2.Lerp(startPosition, endPosition, currentTimeOnPath / totalTimeForPath);

		if (gameObject.transform.position.x == endPosition.x && gameObject.transform.position.y == endPosition.y)
		{
			if (currentWaypoint < waypoints.Length - 2)
			{
				currentWaypoint++;
				lastWaypointSwitchTime = Time.time;
				RotateIntoMoveDirection();
			}
			else
			{
				

				AudioSource audioSource = gameObject.GetComponent<AudioSource>();
				AudioSource.PlayClipAtPoint(audioSource.clip, transform.position, 1f);
				gameObject.SetActive(false);
				Destroy(gameObject, 2);
				gameManager.Live--;

			}
		}
	}

	private void RotateIntoMoveDirection()
	{
		//1
		Vector3 newStartPosition = waypoints[currentWaypoint].position;
		Vector3 newEndPosition = waypoints[currentWaypoint + 1].position;
		Vector3 newDirection = (newEndPosition - newStartPosition);
		//2
		float x = newDirection.x;
		float y = newDirection.y;
		float rotationAngle = Mathf.Atan2(y, x) * 180 / Mathf.PI;
		//3
		//GameObject sprite = gameObject.transform.Find("Sprite").gameObject;
		transform.rotation = Quaternion.AngleAxis(rotationAngle, Vector3.forward);
	}
}
