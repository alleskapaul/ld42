﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttackController  {

	GameObject Target { get; set; }
	Vector3 StartPosition { get; set; }
	Vector3 TargetPosition { get; set; }
	int Damage { get; set; }
	float Range { get; set; }
}
