﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAttackController : MonoBehaviour, IAttackController {

	public GameObject Target { get; set; }
	public Vector3 StartPosition { get; set; }
	public Vector3 TargetPosition { get; set; }
	public int Damage { get; set; }
	public float Range { get; set; }
	public float speed;

	private float distance;
	private float startTime;
	private GameManager gameManager;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
		distance = Vector2.Distance(StartPosition, TargetPosition);
		GameObject gm = GameObject.Find("GameManager");
		gameManager = gm.GetComponent<GameManager>();

		//rotate the bullet to the enemy
		Vector3 direction = StartPosition - TargetPosition;
		transform.rotation = Quaternion.AngleAxis(
			Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI,
			new Vector3(0, 0, 1));
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float timeInterval = Time.time - startTime;
		transform.position = Vector3.Lerp(StartPosition, TargetPosition, timeInterval * speed / distance);

		if (transform.position.Equals(TargetPosition))
		{
			if (Target != null)
			{
				var enemy = Target.GetComponent<EnemyController>();
				enemy.Health -= Damage;
				if (enemy.Health <= 0 && enemy.isActiveAndEnabled)
				{
					
					AudioSource audioSource = Target.GetComponent<AudioSource>();
					AudioSource.PlayClipAtPoint(audioSource.clip, transform.position);

					Target.SetActive(false);
					Destroy(Target, 2);

					gameManager.Gold += enemy.value;
				}
			}
			Destroy(gameObject);
		}
	}
}
