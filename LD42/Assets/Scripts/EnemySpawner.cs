﻿using UnityEngine;
using System.Linq;
using System;

[Serializable]
public class Wave
{
	public GameObject enemyPrefab;
	public float spawnInterval = 2;
	public int maxEnemies = 20;
}

public class EnemySpawner : MonoBehaviour {

	public Wave[] waves;
	public int timeBetweenWaves = 5;

	private GameManager gameManager;

	private float lastSpawnTime;
	private int enemiesSpawned = 0;

	public GameObject enemyPrefab;

	private Transform[] waypoints;
	// Use this for initialization
	void Start () {
		lastSpawnTime = Time.time;
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

		var points = GetComponentsInChildren<Transform>();
		waypoints = points.Where(t => t.name != "Path").ToArray();

		gameManager.TotalWave = waves.Length;

		//Instantiate(enemyPrefab).GetComponent<EnemyController>().waypoints = o.ToArray();
	}

	private void FixedUpdate()
	{
		int currentWave = gameManager.Wave;
		if (currentWave < waves.Length)
		{
			float timeInterval = Time.time - lastSpawnTime;
			float spawnInterval = waves[currentWave].spawnInterval;
			if (((enemiesSpawned == 0 && timeInterval > timeBetweenWaves) ||
				 timeInterval > spawnInterval) &&
				enemiesSpawned < waves[currentWave].maxEnemies)
			{
				lastSpawnTime = Time.time;
				GameObject newEnemy = Instantiate(waves[currentWave].enemyPrefab, waypoints[0].position, waypoints[0].rotation);
				newEnemy.GetComponent<EnemyController>().waypoints = waypoints;
				enemiesSpawned++;
			}

			if (enemiesSpawned == waves[currentWave].maxEnemies &&
				GameObject.FindGameObjectWithTag("Enemy") == null)
			{
				gameManager.Wave++;
				enemiesSpawned = 0;
				lastSpawnTime = Time.time;
			}
		}
		else
		{
			gameManager.WinGame();
		}
	}

}
