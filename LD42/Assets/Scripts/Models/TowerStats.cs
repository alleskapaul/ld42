﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TowerStats {
	public GameObject TowerPrefab;
	public Sprite icon;
	public int BuyValue;
	public int SellValue;

}
