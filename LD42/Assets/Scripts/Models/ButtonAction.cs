﻿using System;
using UnityEngine;

[Serializable]
public class ButtonAction {

	public int value = 0;
	public GameObject TowerPrefab;
	public Sprite icon;
}
